#//include <iostream>
#include <assert.h>

#include "../include/hdd.h"
#include "../include/mbr.h"
#include "../include/pentry.h"
#include "../include/ext2fs.h"


using namespace std;

void test_file_system(Ext2FS * fs){
	cout << "=== Filesystem Superblock ===" << endl;
	cout << *(fs->superblock()) << endl;

	//Verifico que la informacion de la cantidad de bloques sea la esperada
	assert(fs->superblock()->blocks_count == 102400);
}

void test_hdd(HDD * hdd){
	unsigned char buffer[SECTOR_SIZE];
	hdd->read(0, buffer);
	MBR mbr(buffer);
	const PartitionEntry & pentry = mbr[1];
	cout << "=== Partition Data ===" << endl;
	cout << pentry << endl << endl;

	//Verifico que la LBA empiece donde se espera
	assert(pentry.start_lba() == 4096);
}

void test_block_groups(Ext2FS * fs){
	cout << "=== Block Groups Data ===" << endl;
	unsigned int block_groups = fs->block_groups();
	for(unsigned int i = 0; i < block_groups; i++)
	{
		cout << *(fs->block_group(i)) << endl;
	}
	Ext2FSBlockGroupDescriptor block_group = *(fs->block_group(1));

	//Verifico que el block group 1 tenga la información correcta
	assert(block_group.block_bitmap == 8195);
}


void list(Ext2FS * fs, struct Ext2FSInode * from, int level = 0)
{
	if(from == NULL)
		from = fs->load_inode(EXT2_RDIR_INODE_NUMBER);
	//std::cerr << *from << std::endl;
	assert(INODE_ISDIR(from));

	unsigned int block_size = 1024 << fs->superblock()->log_block_size;
	unsigned char buffer[block_size];
	unsigned int i = 0;
	while (i*block_size < from->size) {
		unsigned int aux = fs->get_block_address(from, i);
		fs->read_block(aux, buffer);
		unsigned int j = 0;
		do {
			struct Ext2FSDirEntry* dir_entry = (struct  Ext2FSDirEntry*) &buffer[j];
			if(dir_entry->name[0] != '.' && dir_entry->inode != 0) {
				for (int i = 0; i < level; i++) cout << "│   ";
				std::cout << "├──" <<  dir_entry->name << std::endl;
				if(INODE_ISDIR(fs->load_inode(dir_entry->inode))) list(fs, fs->load_inode(dir_entry->inode), level+1);
			}
			j += dir_entry->record_length;
		} while (j < block_size);
		i++;
	}
	return;
}

int main(int argc, char ** argv)
{
	HDD hdd(argv[1]);

	//Esto lo pueden comentar. Es sólo para ver si descomprimieron bien la imagen de disco
	//test_hdd(&hdd);

	Ext2FS * fs = new Ext2FS(hdd, 1);

	//Esto lo pueden comentar. Es sólo para ver si el FS está bien
	//test_file_system(fs);

	//Esto lo pueden comentar. Es sólo para ver si el FS está bien
	//test_block_groups(fs);

	//TODO: Completar el código del último ejercicio acá
	struct Ext2FSInode * inodo = fs->get_file_inode_from_dir_inode(NULL, "grupos");
	inodo = fs->get_file_inode_from_dir_inode(inodo, "g2");
	inodo = fs->get_file_inode_from_dir_inode(inodo, "nota.txt");
	unsigned int block_size = 1024 << fs->superblock()->log_block_size;
	unsigned int msg_lba = fs->get_block_address(inodo, 14000/block_size);
	unsigned char msg[block_size];
	fs->read_block(msg_lba, msg);
	for (int i = 0; i < 50; i++)
		cout << msg[14000 % block_size + i - 1];

	cout << endl << endl ;
	list(fs, NULL);

	return 0;
}
